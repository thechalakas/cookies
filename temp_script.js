function hello_there2()
{
    <!-- This is one of the many ways to print an output via javascript -->
    document.getElementById("dom_1").innerHTML = "welcome to js training (from the external js file)";
}

function setCookie() 
{
    var cname = "cookie1";
    var cvalue="hello jay";
    var exdays=2;
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) //send name of cookie, like cookie1
{
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);  //collect all the cookies
    var ca = decodedCookie.split(';');  //split them up
    for(var i = 0; i < ca.length; i++)  //loop through them. 
    {
        var c = ca[i];
        while (c.charAt(0) == ' ') 
        {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) //find the cookie with the cookie name we provided as parameter.
        {
            return c.substring(name.length, c.length);  //if found, return that. 
        }
    }
    return "";
}

function checkCookie() 
{
    var username = getCookie("cookie1");  //get cookie name from all the cookies. 
    if (username != "") //its not null
    {
        alert("Welcome again " + username); //show the value
    }
    else 
    {
        alert("cookie not found");
    }
}